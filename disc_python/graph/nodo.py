class Nodo:

    def __init__(self, index, **kwargs):
        self.index = index
        for key, value in kwargs.items():
            setattr(self, key, value)

    def __repr__(self):
        return str(self.__dict__)
