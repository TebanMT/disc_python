from disc_python.graph import nodo

class Graph:

    def __init__(self, dirigido=True):
        self.graph = dict()
        self._nodos = dict()
        self._adj = dict()
        self._matrix = []
        self._dirigido = dirigido

    def add_node(self, index, **kwargs):
        node = nodo.Nodo(index, **kwargs)
        self._nodos[index] = node

    def add_edge(self, u, v, **kwargs):
        if u not in self._nodos:
            node = nodo.Nodo(u)
            self._nodos[u] = node
            self._adj[u] = dict()
        if v not in self._nodos:
            node = nodo.Nodo(v)
            self._nodos[v] = node
            self._adj[v] = dict()
        # add the edge
        datadict = self._adj[u].get(v, dict())
        datadict.update(kwargs)
        self._adj[u][v] = datadict
        if not self._dirigido:
            self._adj[v][u] = datadict

    def to_matrix(self):
        print(len(self._nodos))
        for i,v in self._nodos.items():
            self._matrix.append([])
            for j,vv in self._nodos.items():
                pass
