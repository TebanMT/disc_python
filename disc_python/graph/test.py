import unittest
from disc_python.graph import graph
import copy
import mock
import pprint

pretty_print = pprint.PrettyPrinter()

class GraphTests(unittest.TestCase):

    def test_add_node(self):
        g = graph.Graph()
        g.add_node(1,termino="perro")
        self.assertEqual(g._nodos[1].termino, "perro")
        self.assertIn(1,g._nodos)
        g.add_node(1,termino="gato")
        self.assertEqual(g._nodos[1].termino, "gato")
        self.assertIn(1,g._nodos)
        g.add_node(2,termino="gator")
        self.assertNotEqual(g._nodos[2].termino, "gato")
        self.assertIn(2,g._nodos)
        #pretty_print.pprint(g._nodos)

    def test_add_edge(self):
        g = graph.Graph()
        g.add_edge(1,2,peso=10)
        #pretty_print.pprint(g._nodos)
        #pretty_print.pprint(g._adj)

    def test_to_matrix(self):
        g = graph.Graph()
        g.add_edge(1,2,peso=10)
        pretty_print.pprint(g._nodos)
        pretty_print.pprint(g._matrix)

if __name__ == "__main__":
    unittest.main()
